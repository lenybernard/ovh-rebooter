import os
import ovh

client = ovh.Client(
    endpoint='ovh-eu',
    application_key=os.getenv('OVH_APP_KEY'),
    application_secret=os.getenv('OVH_APP_SECRET'),
    consumer_key=os.getenv('OVH_CONSUMER_KEY'),
)

result = client.post('/vps/{vpsId}/reboot'.format(vpsId=os.getenv('OVH_VPS_ID')))